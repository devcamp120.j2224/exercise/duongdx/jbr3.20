package com.devcamp.s10.jbr3_20;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SplitStringAPI {
    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String> split(){
        ArrayList<String> arrayList1 = new ArrayList<>();
        String Str = "red,green,blue,brown,pink";
        String[] arrOfStr = Str.split(",");
        for (String a : arrOfStr){
            arrayList1.add(a);
        }
           
       return arrayList1;

    }
}
