package com.devcamp.s10.jbr3_20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr320Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr320Application.class, args);
	}

}
